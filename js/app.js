const filmsList = document.querySelector('.films_list');
const request = fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(data =>
    data.forEach(({ id, name, episodeId, openingCrawl, characters }) => {
      filmsList.insertAdjacentHTML(
        'beforeend',
        `<li><h2 class="film_name">${id}. "${name}"</h2>
        <ul id="${id}">Characters:
            
        </ul>
    <h3>episode №${episodeId}</h3>
    <p style="font-style: italic;">"${openingCrawl}"</p></li>`
      );
      characters.forEach(character => {
        fetch(character)
          .then(response => response.json())
          .then(character => {
            const filmId = document.getElementById(id);
            const liCharacterName = document.createElement('li');
            liCharacterName.textContent = character.name;
            filmId.append(liCharacterName);
          });
      });
    })
  );